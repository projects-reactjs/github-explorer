import React, {useEffect, useState} from 'react';
import {useRouteMatch, Link} from 'react-router-dom';
import api from '../../services/api';
import logo from '../../assets/logo.svg';
import {FiChevronLeft, FiChevronRight} from 'react-icons/fi';
import {Header, RepositoryInfo, Issues} from './styles';

interface RepositoryParams {
  repository: string;
}

interface Repository {
  full_name: string;
  description: string;
  stargazers_count: number;
  forks_count: number;
  open_issues_count: number;
  owner: {
    login: string,
    avatar_url: string
  };
}

interface Issue {
  id: number;
  title: string;
  user: {
    login: string;
  };
  html_url: string;
}

const Repository: React.FC = () => {
  const {params} = useRouteMatch<RepositoryParams>();

  const [repository, setRepository] = useState<Repository | null>(null);
  const [issues, setIssues] = useState<Issue[]>([])

  useEffect(() => {

    async function loadData(): Promise<void> {

      const [repository, issues] = await Promise.all([
        api.get(`/repos/${params.repository}`),
        api.get(`/repos/${params.repository}/issues`)
      ]);

      setRepository(repository.data);
      setIssues(issues.data);
    }

    loadData();

  }, [params.repository]);

  return (
    <>
      <Header>
        <img src={logo} alt="Github Explores" />
        <Link to="/">
          <FiChevronLeft size={16} />
          Voltar ao dashboard
        </Link>
      </Header>

      {
        repository && (
          <RepositoryInfo>
            <header>
              <img alt={repository.owner.login} src={repository.owner.avatar_url} />
              <div>
                <strong>{repository.full_name}</strong>  
                <p>description</p>
              </div>
            </header>
            <ul>
              <li>
                <strong>{repository.stargazers_count}</strong>
                <span>Stars</span>
              </li>
              <li>
                <strong>{repository.forks_count}</strong>
                <span>Forks</span>
              </li>
              <li>
                <strong>{repository.open_issues_count}</strong>
                <span>Issues Abertas</span>
              </li>
            </ul>
          </RepositoryInfo>
        )
      }

      {
        issues.map( issue => (
          <Issues key={issue.id} >
            <a href={issue.html_url} target="__blank" >
              <div>
                <strong>{issue.title}</strong>
                <p>{issue.user.login}</p>
              </div>
              <FiChevronRight size={30}/>
            </a>
          </Issues>
        ))
      }

      

    </>
  )
}

export default Repository;