import React, {useState, FormEvent, useEffect} from 'react';
import {FiChevronRight} from 'react-icons/fi';
import { toast, ToastContainer } from 'react-toastify';
import {Link} from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.min.css'; 
import api from '../../services/api';
import logo from '../../assets/logo.svg';
import { Title, Form, Repositories} from './styles';

interface Repository {
  full_name: string;
  description: string;
  owner: {
    login: string,
    avatar_url: string
  };
}

const Dashboard: React.FC = () => {
  const [newRepo, setNewRepo] = useState('')
  const [repositories, setRepositories] = useState<Repository[]>( () => {
    const storedRepositories = localStorage.getItem('@github-explorer:repositories')

    if(storedRepositories) {
      return JSON.parse(storedRepositories);
    } else {
      return [];
    }
  });

  async function handleAddRepository(event: FormEvent<HTMLFormElement>): Promise<void> {
    event.preventDefault();
    if(!newRepo) {      
      toast.error('Digite o Usuario/repositorio!');
      return;
    }
    
    try {
      const response = await api.get<Repository>(`/repos/${newRepo}`);

      
      const repository = response.data;
      toast.success('Sucesso!');
      setRepositories([...repositories, repository] );
      setNewRepo('');

    } catch (error) {
      toast.error('Usuario/repositorio não encontrado!');
      return;
    }
  }

  useEffect( () => {
    localStorage.setItem('@github-explorer:repositories',JSON.stringify(repositories) )
  }, [repositories]);

  return (
    <>
      <ToastContainer />
      <img src={logo} alt="Github Explorer"/>
      <Title>Explore Repositories on Github</Title>

      <Form onSubmit={handleAddRepository} >
        <input value={newRepo} onChange={e => setNewRepo(e.target.value)} type="text" placeholder="Type here the repository name"/>
        <button type="submit">Pesquisar</button>
      </Form>

      <Repositories>
        {
          repositories.map( rep => (
            <Link key={rep.full_name} to={`/repository/${rep.full_name}`}  >
              <img src={rep.owner.avatar_url} alt={rep.owner.login} />
              <div>
                <strong>{rep.full_name}</strong>
                <p>{rep.description}</p>
              </div>
              <FiChevronRight size={30}/>
            </Link>
          ) )
        }
        
      </Repositories>


    </>
  );
};

export default Dashboard;
