import styled from 'styled-components';
import {shade} from 'polished';

export const Title = styled.h1`
  font-size: 48px;
  text-decoration-color: #3a3a3a;
  margin-top: 80px;
  max-width: 450px;
  line-height: 56px;
`;

export const Form = styled.form`
  margin-top: 40px;
  max-width: 700px;
  display: flex;

  input {
    flex: 1;
    height: 50px;
    padding: 0 24px;
    border: 0;
    border-radius: 5px 0 0 5px;
    color: #3a3a3a;

    &::placeholder {
      color: #a8a8b3;
    }
  }
  button {
    width: 210px;
    height: 50px;
    background-image: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
    border-radius: 0px 5px 5px 0px;
    border: 0;
    color: #FFF;
    font-weight: bold;
    transition: background-color 0.2s;
    
    &:hover{
      background-image: linear-gradient(24deg, #2cb5e8 0%, #1fc8db 51%, #9fb8ad 95%);
    }

  }

`;

export const Repositories = styled.div`
    margin-top: 80px;
    max-width: 700px;

        
    a {
      background: #FFF;
      border-radius: 5px;
      padding: 24px;
      width: 100%;
      text-decoration: none;
      display: flex;
      align-items: center;
      transition: transform 0.3s;

      &:not(:first-child) {
        margin-top: 16px;
      }
      
      &:hover{
        transform: translateX(10px);
        background: ${shade(0.2, '#FFF')}
      }

      img{
        width: 64px;
        height: 64px;
        border-radius: 50%;
      }

      div{
        margin-left: 16px;

        strong{
          font-size: 20px;
          color: #3a3a3a;        
        }
        p{
          font-size: 18px;
          color: #a8a8b3;
          margin-top: 4px;
        }
        
      }
      svg {
        margin-left: auto;
        color: #a8a8b3;
      }

    }
    
    
`;
