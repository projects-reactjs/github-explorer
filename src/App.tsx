import React from 'react';
import Routes from './routes';
import {BrowserRouter} from 'react-router-dom';
import GlobalStyles from './styles/globalStyles';

const App: React.FC = () => (
  <BrowserRouter>
    <GlobalStyles />
    <Routes />
  </BrowserRouter>
  
)

export default App;
